def tickets(items):
    current_time = 0
    index = 0
    time_taken = [0] * len(items)
    iterations = 0
    while True:
        iterations += 1
        if iterations > 15:
            print("Damn...")
            break
        if items == [0] * len(items):
            break
        if items[index] == 0:
            index += 1
            if index == len(items):
                index = 0
            continue
        print(items)
        current_time += 1
        items[index] -= 1
        if items[index] == 0:
            time_taken[index] = current_time
        index += 1
        if index == len(items):
            index = 0
    return time_taken
