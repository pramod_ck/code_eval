# Code Checker

## A script which runs a command and checks if its output matches expected output

____________________

### Usage Example:

```
python code_checker.py -c "python hello.py" -o hello.txt -t 100
```

Code Checker takes 3 arguments:

* `--command`(`-c`): The command to be executed

* `--output`(`-o`): Path to file containing expected output

* `--timeout`(`-t`): Time-limit in seconds
