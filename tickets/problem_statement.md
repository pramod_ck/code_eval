## Tickets 

There's a ticket-seller who sells movie tickets at a ticket-counter, where customers stand in a queue and buy one ticket at a time. However, if the same customer needs to buy another ticket, then that customer has to go back to the end of the queue.

Write a program that outputs how much time each customer needs to buy all her tickets.  

For example, if we have three customers:

```
a 3
b 1
c 1
```

where `a` is at the front of the queue and needs 3 tickets, `b` is up next and needs `1` tickets and `c` needs 1 ticket, then the total time required for `a` to buy all her tickets is 5 units of time, `b` needs 2 units of time, and c needs `3` units of time.

Your output should be:

```
a 5
b 2
c 3
```

__________________


Write your program in `tickets.py` so that it reads the input from `movie_buffs.txt`
