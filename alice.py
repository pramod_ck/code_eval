from collections import Counter
import sys

with open('alice_chap_1.txt', 'r') as f:
    s = f.read()

words = s.split(' ')
counter = Counter(words)


class Key(object):
    def __init__(self, obj, *args):
        self.word, self.count = obj

    def __lt__(self, other):
        if self.count > other.count:
            return True
        if self.count < other.count:
            return False
        return self.word < other.word

    def __gt__(self, other):
        if self.count < other.count:
            return True
        if self.count > other.count:
            return False
        return self.word > other.word

    def __eq__(self, other):
        return False

    def __le__(self, other):
        return False

    def __ge__(self, other):
        return False

    def __ne__(self, other):
        return True


sorted_items = sorted(counter.items(), key=Key)
for word, count in sorted_items[0:20]:
    print('{} {}'.format(word, count))
