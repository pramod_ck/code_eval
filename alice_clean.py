"""
Cleans the original alice.txt file fetched from Project Gutenberg
by removing punctuation marks, extra spaces, and other extraneous characters.
"""


def clean(input_file_path, output_file_path):
    with open(input_file_path, 'r') as f:
        s = f.read()
    funcs = [
        strip_and_lower,
        split_into_words,
        remove_empty_words,
        remove_double_dashes,
        remove_empty_words,
        remove_last_special_chars,
        remove_empty_words,
        remove_first_special_chars,
        remove_empty_words
    ]
    result = s
    for f in funcs:
        result = f(result)
    with open(output_file_path, 'w') as f:
        s = ' '.join(result)
        f.write(s)
    return result


def strip_and_lower(text):
    return text.strip().lower()


def split_into_words(text):
    words = text.split(' ')
    words2 = []
    for w in words:
        if '\n' in w:
            words2.extend(w.split('\n'))
        else:
            words2.append(w)
    return words2


def remove_empty_words(words):
    return list(filter(lambda x: x != '', words))


def remove_double_dashes(words):
    words2 = []
    for word in words:
        if '--' in word:
            w2 = word.split("--")
            words2.extend(w2)
        else:
            words2.append(word)
    return words2


def remove_last_special_chars(words):
    words2 = []
    for word in words:
        last_char_ord = ord(word[-1])
        if last_char_ord < ord('a') or last_char_ord > ord('z'):
            word = word[0:-1]
        words2.append(word)
    return words2


def remove_first_special_chars(words):
    words2 = []
    for word in words:
        first_char_ord = ord(word[0])
        if first_char_ord < ord('a') or first_char_ord > ord('z'):
            word = word[1:]
        words2.append(word)
    return words2
