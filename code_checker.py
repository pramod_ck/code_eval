import argparse
from concurrent.futures import ProcessPoolExecutor
import subprocess
import time


def check(command, expected_output):
    try:
        output = subprocess.check_output(command, shell=True)
    except subprocess.CalledProcessError:
        return False
    str_output = output.decode('utf-8')
    # print("************* Output received: *******************", str_output)
    # print("************* Expected output: *******************", expected_output)
    # import pdb; pdb.set_trace()
    return str_output.strip() == expected_output.strip()


def check_with_timeout(command, expected_output, timeout):
    pool = ProcessPoolExecutor()
    future = pool.submit(check, command, expected_output)
    for _ in range(0, timeout * 100):
        if future.done():
            return {'result': future.result(), 'time_exceeded': False}
        time.sleep(1 / 100)
    return {'result': None, 'time_exceeded': True}


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Code Checker')
    parser.add_argument('-c', '--command', required=True)
    parser.add_argument('-o', '--output_file_path', required=True)
    parser.add_argument('-t', '--timeout', required=True)

    args = vars(parser.parse_args())
    command = args['command']
    output_file_path = args['output_file_path']
    timeout = int(args['timeout'])

    with open(output_file_path, 'r') as f:
        expected_output = f.read().strip()
    output = check_with_timeout(command, expected_output, timeout)
    if output['result'] is True:
        print("✔ Test case passed!")
    elif output['time_exceeded'] is True:
        print('❌ Time Exceeded')
    else:
        print("❌ Test case failed")
