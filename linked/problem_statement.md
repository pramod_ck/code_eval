# Remove items greater than `x` from a linked list

Given a linked list, write a function that takes the head of a linked-list as input and removes all elements greater than a specified number, `x` from the linked-list.

_______________


Write your code in `linked.py`, and utilize the class `Node` to construct each node in your linked list

Your program should: 

1. Read the file `linked.txt` which contains the input. The input has two lines. The first line consists of a bunch of integers separated by spaces. The second line contains the value, `x`.
2. Construct a linked list contains all the numbers. 
3. Remove all the nodes whose values are greater than `x`. 
4. Iterate through the linked list and print the remaining numbers


# Example Input:

```
10 1 5 4 2
6
```

# Expected Output

```
1 5 4 2
```

_____________


Write your code in `linked.py` and read the input from `linked.txt`

