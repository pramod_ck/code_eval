## Word Frequency

Write a program that prints the 20 most common words from the first chapter of the book "Alice in Wonderland", along with the number of occurrences of each of these words. 

If two or more words have the same number of occurrences, then sort these words in lexicographic order. 

You can find all the words at `alice_chap_1.txt`. Write your code in `alice.py`


```
python3 alice.py
```

### Expected Output Format:

```
word1 count1
word2 count2
```

and so on...
